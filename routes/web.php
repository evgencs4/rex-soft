<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'IndexController@index'); //start page route (main page)
//ajax on main page//
Route::post('ajax/load', 'IndexController@loadData');//load data
Route::post('ajax/saveCategory', 'IndexController@saveCategory');//save Category
Route::post('ajax/postTask', 'IndexController@postTask');//post task (create task, update task)
Route::post('ajax/deleteTask', 'IndexController@deleteTask');//delete task
Route::post('ajax/statusTask', 'IndexController@statusTask');//status task
