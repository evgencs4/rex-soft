@extends('index')
@section('page-title')
To-Do List
@endsection
@section('includable')
    <link type="text/css" rel="stylesheet" href="/js/toastr/toastr.min.css">
    <link type="text/css" rel="stylesheet" href="/js/sweetalert/sweet-alert.css">
    <link type="text/css" rel="stylesheet" href="/js/ladda-bootstrap/ladda.min.css">
@endsection
@section('content')
<div class="container">

    <!-- NAVBAR START -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">TO-DO List</a>
            </div>
        </div>
    </nav>
    <!-- NAVBAR END -->

    <!-- CONTENT START -->
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading lead clearfix">
                    Categories
                    <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#create_category_modal">
                        Create New Category
                    </button>
                </div>
                <div id="categories-container" class="panel-body list-group">
                    <img style="display: block; margin: 0 auto;" src="/img/25.gif">{{--preloader, inline style only test task variant code--}}
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading lead clearfix">
                    Tasks
                    <button id="create_task_modal_button" type="button" class="btn btn-success pull-right" data-toggle="modal" {{--data-target="#create_task_modal"--}}>
                        Create New Task
                    </button>
                </div>
                <div class="panel-body">
                    <ul id="task-container" class="todo-list ui-sortable">
                        <img style="display: block; margin: 0 auto;" src="/img/25.gif">{{--preloader, inline style only test task variant code--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- CONTENT END -->

</div>

<!-- CATEGORY MODAL START -->
<div id="create_category_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Create New Category</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>List Name</label>
                        <input type="text" class="form-control" placeholder="List Name" autocomplete="off">{{--autocomplete="off"--}}
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <button id="category-save" type="button" class="btn btn-primary ladda-button" data-style="slide-up" data-color="blue" data-size="xs">
                    <span class="ladda-label">Save changes</span>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- CATEGORY MODAL END -->

<!-- TASK MODAL START -->
<div id="create_task_modal" class="modal fade" tabindex="-1" role="dialog" event="" task_id="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Task</label>
                        <input type="text" class="form-control" placeholder="Task" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select id="task_categories_select" class="form-control" autocomplete="off"></select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <button id="save_task_button" type="button" class="btn btn-primary ladda-button" data-style="slide-up" data-color="blue" data-size="xs">
                    <span class="ladda-label">Save changes</span>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- TASK MODAL END -->
@endsection
@section('scripts')
<script type="text/javascript" src="/js/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/js/sweetalert/sweet-alert.min.js"></script>
<script type="text/javascript" src="/js/ladda-bootstrap/spin.min.js"></script>
<script type="text/javascript" src="/js/ladda-bootstrap/ladda.min.js"></script>
<script type="text/javascript" src="/js/ladda-bootstrap/ladda.jquery.min.js"></script>
<script>
jQuery(document).ready(function () {

    loadData(); //load data when page start
    var categoryLaddas = jQuery('#category-save').ladda();// init button animate
    var taskLaddas = jQuery('#save_task_button').ladda();

    //TODO:: write normal key press logic.
    // i disable this event...... Maybe I'm wrong. But it annoys me.
    jQuery('input[type=text]').on('keypress', function(e){
        if(e.which==13)e.preventDefault();
    });
    jQuery('select').on('keypress', function(e){
        if(e.which==13)e.preventDefault();
    });

    function loadData() { // load data, load category and task.

        jQuery.ajax({
            url: "{{ url('/ajax/load') }}",
            type: "post",
            dataType: "json",
            data: "_token={{csrf_token()}}",

            beforeSend: function () {
                jQuery('.please-wait-category').show(); //show preloader
                jQuery('.please-wait-task').show();
            },
            success: function(result)
            {
                jQuery('.please-wait-category').hide();//hide preloader
                jQuery('.please-wait-task').hide();

                if(result){
                    var htmlCategoriesBlock = ``; // `` = not cross browser code, only test task variant.
                    var htmlTaskBlock = ``;
                    var htmlTaskCategoriesSelect = `<option value="" selected="selected" ></option>`;

                    //append total block
                    htmlCategoriesBlock += `<a href="javascript:;" class="list-group-item active"><span class="badge">${result.categoriesTotal}
                            </span>All</a>`;
                    //append categories and Task Categories Select
                    jQuery.each(result.categories, function (key, val) {
                        htmlCategoriesBlock += `<a href="javascript:;" class="list-group-item"><span class="badge">${val.taskTotal}</span>${val.name}</a>`;
                        htmlTaskCategoriesSelect += `<option value="${val.id}" >${val.name}</option>`;
                    });
                    //append task
                    var status = '';
                    var checked = '';
                    jQuery.each(result.tasks, function (key, val) {
                        status = (!val.status)?'class="done"':'';
                        checked = (val.status)?'checked="checked"':'';
                        htmlTaskBlock += `<li ${status}>
                                            <input class="active-check" attr_status="${val.status}" attr_id="${val.id}" type="checkbox" ${checked} value="">
                                            <span class="text">${val.name}</span>
                                            <small class="label label-danger">${val.categoryName}</small>
                                            <div class="tools">
                                                <i attr_id="${val.id}" attr_cat_id="${val.categoryId}" attr_task_name="${val.name}" class="glyphicon glyphicon glyphicon-pencil"></i>
                                                <i attr_id="${val.id}" class="glyphicon glyphicon-remove-circle"></i>
                                            </div>
                                        </li>`;
                    });
                    //insert html in blocks
                    jQuery('#categories-container').html(htmlCategoriesBlock);
                    jQuery('#task-container').html(htmlTaskBlock);
                    jQuery('#task_categories_select').html(htmlTaskCategoriesSelect);
                    initEvent(); //init event when content load
                }
            },
            error: function () {
                alert('whoops something broke. page force reload.');
                location.reload();
            }
        });
    } //loadData() end


    jQuery('#category-save').on('click', function () { // category save button handler

        var that = jQuery(this);
        var inputValue = that.closest('.modal-content').find('input[type=text]').val();

        jQuery.ajax({
            url: "{{ url('/ajax/saveCategory') }}",
            type: "post",
            dataType: "json",
            data: "categoryName="+inputValue+"&_token={{csrf_token()}}",
            beforeSend: function () {
                categoryLaddas.ladda('start');
            },
            success: function(result)
            {
                if(result.result=='success'){
                    jQuery('#create_category_modal').modal('hide');
                    swal("Success!", "Category create!", "success");
                    loadData();

                } else {
                    jQuery.each(result, function(key, val){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-bottom-left",
                            "onclick": null,
                            "showDuration": "1000",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.error(val, "Error");
                    });
                }
                categoryLaddas.ladda('stop');
            },
            error: function () {
                categoryLaddas.ladda('stop');
                alert('whoops something broke. page force reload.');
                location.reload();
            }
        });
    });


    jQuery('#create_task_modal_button').on('click', function () { // new task button event

        var modalWindowTask = jQuery('#create_task_modal');
        modalWindowTask.find('input[type=text]').val('');//clear input
        $('#task_categories_select').val('').change(); //clear select
        //inset h4 text, create
        modalWindowTask.find('.modal-title').html('Create New Task');
        //refresh attr
        modalWindowTask.attr('event', 'save');
        modalWindowTask.attr('attr_id', '');
        modalWindowTask.modal();

    });


    jQuery('#save_task_button').on('click', function () { //save task, update task

        var modalWindowTask = jQuery('#create_task_modal');
        var event = modalWindowTask.attr('event');
        var taskName = modalWindowTask.find('input[type=text]').val();
        var categoryId = modalWindowTask.find('#task_categories_select option:selected').val();
        if(event == 'update') var taskId = modalWindowTask.attr('task_id');

        var formAjaxData = new FormData;
        formAjaxData.append("_token", "{{ csrf_token() }}");
        formAjaxData.append("event", event);
        if(event == 'update') formAjaxData.append("task_id", taskId); // if event update append new param
        formAjaxData.append("name", taskName);
        formAjaxData.append("categorie_id", categoryId);

        jQuery.ajax({
            url: "{{ url('/ajax/postTask') }}",
            type: "post",
            data: formAjaxData,
            processData: false,
            cache: false,
            contentType: false,
            dataType: "json",
            beforeSend: function () {
                taskLaddas.ladda('start');
             },
            success: function(result)
            {
                if(result.result == 'success'){
                    var msg = '';
                    (result.event == 'save')? msg = 'Task save' : msg = 'Task update';
                    jQuery('#create_task_modal').modal('hide');
                    swal("Success!", msg, "success");
                    loadData();

                } else {
                    jQuery.each(result, function(key, val){
                        toastr.options = {
                            "closeButton": true,
                            "positionClass": "toast-bottom-left",
                            "onclick": null,
                            "showDuration": "1000",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.error(val, "Error");
                    });
                }
                taskLaddas.ladda('stop');
            },
            error: function () {
                taskLaddas.ladda('stop');
               alert('whoops something broke. page force reload.');
               location.reload();
            }
        });


    });

    function initEvent(){ // init event on page

        jQuery('.glyphicon-remove-circle').on('click', function(){ //delete task handler

            var that = jQuery(this);
            var taskId = that.attr('attr_id');

            swal({ //alert confirmation message
                title: "Are you sure?",
                text: "task will be removed",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: true
            }, function (isConfirm) {

                if (isConfirm) { //if user confirm go delete task
                    jQuery.ajax({
                        url: "{{ url('/ajax/deleteTask') }}",
                        type: "post",
                        dataType: "json",
                        data: "taskId="+taskId+"&_token={{csrf_token()}}",
                        beforeSend: function () {
                            jQuery('.glyphicon-remove-circle').off('click');//off  delete event
                        },
                        success: function(result)
                        {
                            if(result.result=='success'){
                                jQuery('#create_category_modal').modal('hide');
                                swal("Success!", "Task delete!", "success");
                                loadData();
                            }
                        },
                        error: function () {
                            alert('whoops something broke. page force reload.');
                            location.reload();
                        }
                    });
                }

            });
        });

        jQuery('.glyphicon-pencil').on('click', function () { // edit task event

            var modalWindowTask = jQuery('#create_task_modal');
            var that = jQuery(this);
            var name = that.attr('attr_task_name');
            var categoryId = that.attr('attr_cat_id');

            modalWindowTask.find('input[type=text]').val(name); //insert name
            $('#task_categories_select').val(categoryId).change(); //change category

            //inset h4 text, edit
            //refresh attr
            modalWindowTask.find('.modal-title').html('Update Task');
            modalWindowTask.attr('event', 'update');
            modalWindowTask.attr('task_id', that.attr('attr_id'));
            modalWindowTask.modal();

        });


        jQuery('.active-check').on('click', function(){ // status handler

            var that = jQuery(this);
            var taskId = that.attr('attr_id');
            var status = that.attr('attr_status');

            jQuery.ajax({
                url: "{{ url('/ajax/statusTask') }}",
                type: "post",
                dataType: "json",
                data: "taskId="+taskId+"&status="+status+"&_token={{csrf_token()}}",
                beforeSend: function () {
                    that.attr('disabled',true); //disable button
                },
                success: function(result)
                {
                    if(result.result=='success'){ //refresh status
                        var newStatus = (!result.newStatus)?true:false;
                        that.closest('li').toggleClass('done', newStatus);
                        that.attr('attr_status', result.newStatus);
                    }
                    that.attr('disabled',false);
                },
                error: function () {
                    that.attr('disabled',false);
                    alert('whoops something broke. page force reload.');
                    location.reload();
                }
            });

        });
    }

});
</script>
@endsection

