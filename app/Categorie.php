<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public function task()
    {
        return $this->hasMany('App\Task');
    }

    public static function createCategory($data){
        
        $rules = [
            'categoryName' => 'required|string|max:50|min:2|unique:categories,name',
        ];
        $messages = [
            'required' => 'Field :attribute required',
        ];
        $fieldsNames = [
            'categoryName' => 'List Name',
        ];
        $validator = \Validator::make($data, $rules, $messages);
        $validator->setAttributeNames($fieldsNames);
        if($validator->passes()){

            $category = new Categorie();
            $category -> name = $data['categoryName'];
            $category -> save();
            return true;

        } else {
            return $validator->errors();
        }
    }
}
