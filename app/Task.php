<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

/*$rules = [
'categoryName' => 'required|string|max:50|min:2|unique:categories,name',
];
$messages = [
'required' => 'Field :attribute required',
];
$fieldsNames = [
'categoryName' => 'List Name',
];*/

    public function categorie()
    {
        return $this->belongsTo('App\Categorie');
    }

    protected static function rules(){
        return [
            'name' => 'required|string|max:50|min:2',
            'categorie_id' => 'required|integer',
        ];
    }
    
    protected static function messages(){
        return [
            'required' => 'Field :attribute required',
        ];
    }
    
    protected static function fieldsNames(){
        return [
            'name' => 'Task',
            'categorie_id' => 'Category',
        ];
    }

    public static function saveTask($data){

        $validator = \Validator::make($data, Task::rules(), Task::messages());
        $validator->setAttributeNames(Task::fieldsNames());
        if($validator->passes()){
            $task = new Task();
            $task -> name = $data['name'];
            $task -> categorie_id = $data['categorie_id'];
            $task -> status = 1;
            $task -> save();
            return true;
        } else {
            return $validator->errors();
        }
    }
    
    public static function updateTask($data){
        
        $validator = \Validator::make($data, Task::rules(), Task::messages());
        $validator->setAttributeNames(Task::fieldsNames());
        if($validator->passes()){
            if(Task::where('id', $data['task_id'])->exists()){
                $task = Task::where('id', $data['task_id'])->first();
                $task -> name = $data['name'];
                $task -> categorie_id = $data['categorie_id'];
                $task -> update();
                return true;
            } else {
                abort(404);
            }
        } else {
            return $validator->errors();
        }
    }

}

