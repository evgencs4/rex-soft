<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Task;

use Illuminate\Http\Request;

class Indexcontroller extends Controller
{
    public function index () { //start page function
        return view('front.main');
    }

    public function loadData (Request $request) { //ajax handler, route: ajax/load

        if($request->ajax()) {
            $getCategories = Categorie::select('id','name')->get();
                $categoriesData = [];
                foreach ($getCategories as $category){
                    $categoriesData [] = ['id' => $category->id,
                                        'name' => $category->name,
                                        'taskTotal' => $category->task->count(),
                                        ];
                }
            $getTasks = Task::select('id', 'name', 'categorie_id', 'status')->get();
                $tasksData = [];
                foreach ($getTasks as $task){
                    $tasksData [] =  ['id' => $task->id,
                                    'categoryId' => $task->categorie_id,
                                    'name' => $task->name,
                                    'status' => $task->status,
                                    'categoryName' => $task->categorie->name,
                                    ];
                }
            $allData  = ['categories' => $categoriesData,
                        'categoriesTotal' => $getCategories->count(),
                        'tasks' => $tasksData,
                        ];

            return json_encode($allData);
        } else {
            abort(404);
        }

    }

    public function saveCategory(Request $request){ //save category

        if($request->ajax()) {
            $data = \Purifier::clean( $request->only('categoryName') ); // clean data
            $result = Categorie::createCategory($data);
            if($result === true){
                return json_encode(['result' => "success"]);
            } else {
                return $result->toJson();
            }
        } else {
            abort(404);
        }
    }

    public function postTask(Request $request){ // create, update, task

        if($request->ajax()) {
            $data = \Purifier::clean( $request->only('event', 'task_id', 'name', 'categorie_id') ); // clean data
            if($data['event'] == 'save'){
                $result = Task::saveTask($data);
                    if($result === true){
                        return json_encode(['result' => "success", 'event' => 'save']);
                    } else {
                        return $result->toJson();
                    }
            } else if ($data['event'] == 'update'){
                $result = Task::updateTask($data);
                    if($result === true){
                        return json_encode(['result' => "success", 'event' => 'update']);
                    } else {
                        return $result->toJson();
                    }
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function deleteTask(Request $request){
        if($request->ajax()) {
            $data = \Purifier::clean( $request->only('taskId') ); // clean data
            if( Task::where('id', $data['taskId'])->exists() ){
                Task::where('id', $data['taskId'])->delete();
                return json_encode(['result' => "success"]);
            } else {
                abrot(404);
            }
        } else {
            abrot(404);
        }
    }


    public function statusTask(Request $request){
        if($request->ajax()) {

            $data = \Purifier::clean( $request->only('taskId', 'status') ); // clean data
            if( Task::where('id', $data['taskId'])->exists() ){

                ($data['status'])? $newStatus = 0 : $newStatus = 1;
                $task = Task::where('id', $data['taskId'])->first();
                $task -> status = $newStatus;
                $task -> update();

                return json_encode(['result' => "success", 'newStatus'=>$newStatus]);

            } else {
                abort(404);
            }
        } else {
            abort(404);
        }

    }
    

}

